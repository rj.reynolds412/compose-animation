/**
 * Created by Jimmy McBride on 2022-12-05
 *
 * Copyright © 2022 Jimmy McBride
 */
package com.rave.composeanimation.components

import androidx.compose.animation.animateColorAsState
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.CornerRadius
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.StrokeCap
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.unit.dp

/**
 * Instagram
 *
 * @author Jimmy McBride on 2022-12-05.
 */
@Composable
fun Instagram(
    size: Int,
    toggleAnimation: Boolean,
) {
    val firstColor by animateColorAsState(targetValue = if (toggleAnimation) Color.Red else Color.Yellow )
    val secondColor by animateColorAsState(targetValue = if (toggleAnimation) Color.Magenta else Color.Magenta)
    val thirdColor by animateColorAsState(targetValue = if (toggleAnimation) Color.Yellow else Color.Red )

    val instagramColors = listOf(
        firstColor,
        secondColor,
        thirdColor
    )

    Canvas(modifier = Modifier
        .size(size.dp)
        .padding(16.dp),
        onDraw = {
            drawRoundRect(
                brush = Brush.linearGradient(colors = instagramColors),
                cornerRadius = CornerRadius(60f, 60f),
                style = Stroke(width = 15f, cap = StrokeCap.Round)
            )
            drawCircle(
                brush = Brush.linearGradient(colors = instagramColors),
                radius = 45f,
                style = Stroke(width = 15f, cap = StrokeCap.Round)
            )
            drawCircle(
                brush = Brush.linearGradient(colors = instagramColors),
                radius = 13f,
                center = Offset(this.size.width * .80f, this.size.height * 0.20f),
            )
        }
    )
}
