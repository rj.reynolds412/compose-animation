package com.rave.composeanimation

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.animateColor
import androidx.compose.animation.core.RepeatMode
import androidx.compose.animation.core.infiniteRepeatable
import androidx.compose.animation.core.rememberInfiniteTransition
import androidx.compose.animation.core.tween
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Button
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.rave.composeanimation.components.Greeting
import com.rave.composeanimation.components.Instagram
import com.rave.composeanimation.ui.theme.ComposeAnimationTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ComposeAnimationTheme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    var toggleAnimation by remember {
                        mutableStateOf(true)
                    }

                    var iconVisibility by remember {
                        mutableStateOf(false)
                    }
                    val infiniteTransition = rememberInfiniteTransition()
                    val rotatingColor by infiniteTransition.animateColor(initialValue = Color.Red,
                        targetValue = Color.Yellow,
                        animationSpec = infiniteRepeatable(
                            animation = tween(2000),
                            repeatMode = RepeatMode.Reverse))

                    Column(
                        horizontalAlignment = Alignment.CenterHorizontally,
                        verticalArrangement = Arrangement.Center,
                    ) {
                        Button(onClick = { iconVisibility = !iconVisibility },
                            shape = RoundedCornerShape(16.dp)) {
                            Greeting("RaveBizz")
                        }
                        AnimatedVisibility(visible = iconVisibility,
                            enter = fadeIn(animationSpec = tween(4000)),
                            exit = fadeOut(animationSpec = tween(4000))) {
                            IconButton(
                                onClick = {
                                    toggleAnimation = !toggleAnimation
                                },
                            ) {
                                Instagram(size = 100, toggleAnimation)
                            }

                        }
                    }
                }
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    ComposeAnimationTheme {
        Greeting("Android")
    }
}
