# Compose Animation

Acceptance Criteria

- [ ] Make instagram icon toggle with enter and exit transitions.
- [ ] Once you click the instagram icon animate the colors in an infinite loop.
- [ ] Clicking the instagram icon again will cause the color animation to stop.

First color should rotate colors as follow: Yellow, Red, Magenta
Second color should rotate colors as follow: Red, Magenta, Yellow
Third color should rotate colors as follow: Magenta, Yellow, Red

